import logging
import asyncio
from tkinter import *
from PIL import Image, ImageTk
import os

log = logging.getLogger(__name__)

class Display():
    
    def __init__(self, loop) -> None:
        self.loop = loop
        self.w = None
        self._is_closed = False
        self.panel = None
        self.curr_img_path = os.path.join(
            os.path.dirname(__file__), '../media/LOADING.jpg')
        self.prev_img_path = self.curr_img_path

    def display(self, img_path):
        log.debug('Richiesta cambio immagine sul display con: {}'.format(img_path))
        self.prev_img_path = self.curr_img_path
        self.curr_img_path = os.path.join(os.path.dirname(__file__), '../{}'.format(img_path))
        return

    async def _refresh(self):
        while not self._is_closed:
            if self.curr_img_path != self.prev_img_path:
                image = ImageTk.PhotoImage(Image.open(self.curr_img_path))
                self.panel.config(image=image)
            self.w.update()
            await asyncio.sleep(1)
    
    def _create_panel(self):
        self.w = Tk()
        self.w.title('GAME')
        w, h = self.w.winfo_screenwidth(), self.w.winfo_screenheight()
        self.w.geometry("%dx%d+0+0" % (w, h))
        self.w.config(bg='#4a7a8c')
        self.panel = Label(
            self.w,
            bg='#d9d8d7'
        )
        self.panel.pack(side="bottom", fill="both", expand="yes")

    def open(self):
        self._create_panel()
        self.w_loop = self.loop.create_task(self._refresh())

    def close(self):
        self._is_closed = True
        self.w.destroy()

    def invalid_action(self):
        pass




        
